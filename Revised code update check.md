### AppApplication Changes:
```
// Chach and adding AppFeatureLifecycleObserver
    @Override
    public void onCreate() {
        super.onCreate();
        PDFViewer.getInstance().init(this);
        ActivityLifecycleObserver.getInstance().register(this);    
    }
    
// Check CryptoUtil.getUuidEncrypt and setSecurityCodeEnc = true
    public ConfigManager getConfigManager() {
        if (configManager == null) {
            configManager = ConfigManager.getInstance(this, ConfigUtil.getSecurityCode(this), CryptoUtil.getUuidEncrypt(this), BuildConfig.DEBUG)
                     .setSecurityCodeEnc(true);
        }
        return configManager;
    }
    

#### MCQPlayOptionFragment see answer flag changes
```
private Bundle getPlayBundle() {
        MCQCategoryProperty catProperty = new MCQCategoryProperty();
        ...
        catProperty.setSeeAnswer(false)
        ...
}
```