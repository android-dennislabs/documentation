### Depricated methods updates ###


# All Updates

### 11-05-22: Admob sdk updated in AdsSdk, compileSdk 31
```
//Update dependency's version
	appcompat = '1.4.1'
    material = '1.6.0'
	
    retrofit_version = '2.9.0'
    retrofit_okhttp_version = '4.9.3'
	
    helper_version = '1.8-alpha12'
    adssdkmaster_version = '1.6-beta09'
    config_version = '3.2-gama15'
	
	
//remove app resource files already exists updated file in adsSdk
	ads_native_unified_card.xml
	native_pager_ad_app_install.xml
```

### Splash Screen Android 12 Update
```
    implementation 'androidx.core:core-splashscreen:1.0.0'


public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
      SplashScreen splashScreen = SplashScreen.installSplashScreen(this);

       super.onCreate(savedInstanceState);

       // Keep the splash screen visible for this Activity
       splashScreen.setKeepOnScreenCondition(() -> true );
       startSomeNextActivity();
       finish();
    }
  ...
}
 
 
     <style name="AppTheme.Splash" parent="Theme.SplashScreen">
        <item name="windowSplashScreenBackground">@color/colorPrimary</item>
        <!-- Use windowSplashScreenAnimatedIcon to add either a drawable or an
             animated drawable. One of these is required. -->
        <item name="windowSplashScreenAnimatedIcon">@drawable/splash_app</item>
        <item name="windowSplashScreenAnimationDuration">200</item>
        <item name="postSplashScreenTheme">@style/ActivityTheme.NoActionBar</item>
    </style>
```

# OLD Updates
```
Making changes in project are: mocker, PYPApp, GateExam, ncertOnlyPdfViewer,
		gkcurrentaffairs, englishVocub, ncertpdfviewer, ytPlayer,  StateBoard, FoundationCourse, govtjobs,
			, aptitude, cbsejunior, freebook, kidslearning, mcq-paid-sdk, whiteboardAndroid, learnenglish,

No changes needed projects are : config, adssdkmaster, pdf-viewer, helper, appfeature, mcq-sdk, loginmaster

Pending projects are (Config changes needed in all projects mentioned below) : 
		
Bugs in Projects are :  dictionaryAndroid


Update Project from Git
0. if use helper library Search ".provider" or ".fileprovider" replace with context.getString(R.string.file_provider)
	Note: if you can declear .provider in manifestFile than also add <string name="file_provider">.provider</string> in 
		your string resource file
1. Remove ProgressDialog
2. Remove AsyncTask
3. Check PDFViewer Api 30 bug shouldAskPermissions find Environment.getExternalStorageDirectory()
	Note: replace Environment.getExternalStorageDirectory() with FileUtils.getFileStoreDirectory(context, folderName);
3.1 Adding in Manifest android:requestLegacyExternalStorage="true"
5. Update Config Deprecated classes (AppConstant, AppPreferences, SupportUtil)
6. Objects.requireNonNull
7. Make changes from note 'Appsfeature lib changes.txt' Serch 'initAppFeature' method
8. Update centerCrop image property DailyUpdateFullDesActivity to fitXY 'DailyUpdates Note.txt'
9. Add CryptoUtil support. Search 'ConfigUtil.getSecurityCode' or 'loadConfigFromServer' or 'RetrofitGenerator.getClient'
	Note: update getConfigManager param with 'CryptoUtil.getUuidEncrypt(sMyApplication)'
10. Make changes from note 'activity_category_changes.txt'
11. Make changes from note 'Update lib Imp Notes.txt'
Update sdk version to Api level 30
```

```
Replace
import android.os.AsyncTask;
with
import com.helper.task.AsyncThread;

Replace All AsyncTask< with AsyncThread<


Replace
com.config.util.SupportUtil
with
ConfigUtil

Replace
import com.config.util.SupportUtil;
with
import com.config.util.ConfigUtil;

Replace
import com.config.util.AppConstant;
with
import com.config.config.ConfigConstant;

Replace
import com.config.util.AppPreferences;
with
import com.config.config.ConfigPreferences;


    private void requestDataFromDB(){
        TaskRunner.getInstance().executeAsync(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                return null;
            }
        }, new TaskRunner.Callback<Void>() {
            @Override
            public void onComplete(Void result) {

            }
        });
    }
	
	Replace
	import android.app.ProgressDialog;
	with
	import com.helper.util.BaseUtil;
	
	BaseUtil.showDialog(this, "Downloading ...", true);
	
	BaseUtil.hideDialog();
	
	
	
	 <ImageView
			android:id="@+id/expandedImage"
			android:layout_width="match_parent"
			android:layout_height="wrap_content"
			android:minHeight="200dp"
			android:scaleType="fitXY"
			android:adjustViewBounds="true"
			app:layout_collapseMode="parallax"
			app:layout_collapseParallaxMultiplier="0.7"/>
			
			
public ConfigManager getConfigManager() {
	if(configManager == null){
		configManager = ConfigManager.getInstance(this, SupportUtil.getSecurityCode(this), CryptoUtil.getUuidEncrypt(sMyApplication)
				, BuildConfig.DEBUG).setSecurityCodeEnc(true);
	}
	return configManager;
}
```